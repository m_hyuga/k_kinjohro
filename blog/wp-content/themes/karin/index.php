<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php  if(is_home()) : // ホームなら  ?>

<div id="MaineImg">

  <img src="http://www.kinjohro.co.jp/images/group/04/img_01.jpg" width="850" height="316" alt="" /></div>
<div id="ContentsArea-index">
  <div id="ContentsArea">
 
<?php get_sidebar(); ?>
<div id="RightEntry">
        <div id="TxtEntry">
        
        <div class="WordBoxL">
        <?php query_posts('page_id=2'); ?>  
<?php if (have_posts()) : ?>  
<?php while (have_posts()) : the_post(); ?>  
	<h3 class="WellTxt"><strong><?php the_title(); ?></strong></h3>
	<div>
		<?php global $more; $more = 0; ?>  
		<?php the_content(__('詳細はこちら')); ?> 
	<div class="clear"></div>
	</div>
<?php endwhile; ?>  
<?php endif; ?>  
        
        </div>
        <div class="WordBoxR">
       	  <img src="http://www.kinjohro.co.jp/images/group/04/img_03.jpg" width="312" height="165" alt="" />
 </div>
         
      </div>
       <div class="clear"><br /></div>
        <h3><img src="http://www.kinjohro.co.jp/images/group/04/img_04.jpg" width="650" height="37" alt="お昼の献立" class="MB30" /></h3>
       
     <table border="0" cellpadding="0" cellspacing="0" class="Wnews">
      <?php query_posts('cat=3'); ?>
<?php if (have_posts()) : ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	 <tr>
    <td class="td01">・<time datetime="<?php the_time('Y年m月j日'); ?>"><?php the_time('Y年m月j日'); ?></time></td>
    <td><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></td>
  </tr>
     
<?php endwhile; // end of the loop. ?>
<?php endif; ?>  

</table>
<br />
<h3><img src="http://www.kinjohro.co.jp/images/group/04/img_04.jpg" width="650" height="37" alt="お昼の献立" class="MB30" /></h3>



 <table border="0" cellpadding="0" cellspacing="0" class="Kplan">
 
 

<?php $posts=get_posts('numberposts=5 & post_type=oshinagaki'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>


	 <tr>
    <th><a href="<?php the_permalink() ?>"><?php the_title(); ?>　・・・・　<?php echo c2c_get_custom('金額', ''); ?></a></th>
   <td><?php  echo c2c_get_custom('アイコン', '<img class="iepngfix" alt="" src="http://www.kinjohro.co.jp/blog/group/files/', '" />'); ?></td>
  </tr>
<?php endforeach; endif; ?>   

</table>  



<br />

      
     <h3><img src="http://www.kinjohro.co.jp/images/group/02/img_18.jpg" width="650" height="38" alt="詳細情報" class="MB30" /></h3>
      
     
        <img src="http://www.kinjohro.co.jp/images/group/04/img_14.jpg" width="238" height="307" class="MB20 TaR" alt="" />
        <div class="TakaL2">
          <table border="0" cellspacing="0" cellpadding="0" summary="詳細情報">
          <tr>
            <th class="TaTh2">住所</th>
            <td class="TdTh2">〒920-0911 石川県金沢市橋場町2-23 </td>
          </tr>
          <tr>
            <th class="TaTh">TEL</th>
            <td class="TdTh1">076-224-2467</td>
          </tr>
          <tr>
            <th class="TaTh">営業時間</th>
            <td class="TdTh1">17:00〜21:00　ランチ　11:00〜14:30　　　<br />
              定休日：火曜日</td>
          </tr>
          <tr>
            <th class="TaTh">平均予算</th>
            <td class="TdTh1">6,000円（通常平均）　<br />
              5,000円（宴会平均）　<br />
              3,500円（ランチ平均）　</td>
          </tr>
          <tr>
            <th class="TaTh">総席数</th>
            <td class="TdTh1">28席</td>
          </tr>
          <tr>
            <th class="TaTh">駐車場</th>
            <td class="TdTh1">有　10台</td>
          </tr>
          <tr>
            <th class="TaTh1">クレジットカード</th>
            <td class="TdTh1">VISA MASTER UC DC UFJ(ミリオン) <br />
              DinersClub AmericanExpress <br />
              JCB NICOS(日本信販)</td>
          </tr>
          <tr>
            <th class="TaTh">サービス料</th>
            <td class="TdTh1">10％（夜のみ）</td>
          </tr>
        </table>
        </div>
      <div>
      <p></p>
      </div>
      
      
      
     
    </div>
    <div id="TopButton" class="clear"><img src="http://www.kinjohro.co.jp/images/office/01/img_16.jpg" width="106" height="29" alt="ページの先頭" onclick="backToTop(); return false;" onkeypress="backToTop(); return false;" /></div>
     
     
     
     
  </div>
</div>








<?php get_footer(); ?>
  <?php elseif(get_post_type() === 'oshinagaki'): ?>
<?php include (TEMPLATEPATH . '/taxonomy-oshinagaki.php'); ?>

  <?php elseif(get_post_type() === 'plan2'): ?>
  
  <?php include (TEMPLATEPATH . '/404.php'); ?>
<?php else : ?>
<?php endif;	// is_home() ?>