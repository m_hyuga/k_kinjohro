<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="shortcut icon" href="<?php info('home_url'); ?>images/index/kinjohro.ico" />
<link href="<?php info('home_url'); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php info('home_url'); ?>css/common.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php info('home_url'); ?>css/news.css" rel="stylesheet" type="text/css" media="all" />
<meta name="verify-v1" content="SujRQHL74WS5RLjCBfoB3M1GVmDIdiLMti9LmsJN9p0=" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<script src="<?php info('home_url'); ?>js/AC_RunActiveContent.js" type="text/javascript"></script>
<script language='javascript' src='<?php info('home_url'); ?>js/pagetop.js' type="text/javascript"></script>
<script language='javascript' src='<?php info('home_url'); ?>js/iepngfix.js' type="text/javascript"></script>

<!-- プルダウン -->
	<script type='text/javascript' src='<?php info('home_url'); ?>js/jquery.js'></script>
   
    
      <script type='text/javascript' src='<?php info('home_url'); ?>js/jquery.droppy.js'></script>
      <link rel="stylesheet" href="<?php info('home_url'); ?>css/pull/droppy.css" type="text/css" />
<script language='javascript' src='<?php info('home_url'); ?>js/jquery_auto.js' type="text/javascript"></script>
<style type="text/css">
<!--
.style8 {font-size: 16px}
-->
</style>
<?php wp_head(); ?>
</head> 
<body>


<!-- headerここから -->
<div id="gnavi">
  <div class="inner">
    <h1><a href="<?php info('home_url'); ?>"><img src="<?php info('home_url'); ?>images/logo.png" width="118" height="149" alt="金城楼"></a></h1>
    <ul id="subnavi">
      <li><a href="<?php info('home_url'); ?>english/index.html">English</a></li>
      <li><a href="<?php info('home_url'); ?>form/index.html">ご予約・お問い合わせ</a></li>
    </ul>
    <ul id="mainnavi">
      <li><a class="ryouri" href="<?php info('home_url'); ?>office/ryouri.html">お食事</a></li>
      <li><a class="lodging" href="<?php info('home_url'); ?>office/lodging.html">ご宿泊</a></li>
      <li><a class="bridal" href="<?php info('home_url'); ?>wedding/index.html">ご婚礼</a></li>
      <li><a class="lunchbox" href="<?php info('home_url'); ?>group/syousai.html">お弁当</a></li>
      <li><a class="onlineshop" href="https://asp.hotel-story.ne.jp/ec/kinjohro/products/list.php" target="_blank">お取り寄せ</a></li>
    </ul>
    <div id="breadcrumb">
      <a href="<?php info('home_url'); ?>">ホーム</a> ＞ 金城樓の季節のご案内
    </div>
  </div>
</div>

<div id="headimg">
  <div class="inner season_img"></div>
</div>
<div id="original_system">
  <div class="inner cleafix">

    <div id="original_system_switch">
      <div id="lodging_panel_sw"><img src="<?php info('home_url'); ?>images/lodging_panel_sw.png"></div>
      <div id="ryouri_panel_sw"><img src="<?php info('home_url'); ?>images/ryouri_panel_sw.png"></div>
    </div>

    <div id="lodging_panel" class="cleafix">
      <div class="lodging_inner clearfix">

        <!-- 宿泊検索システムここから -->
        <form id="frmplanseek" name="frmplanseek" onsubmit="return false;">
        <div id="booking_inner">

        <div class="lodging_date">   
          <div class="tit">ご宿泊日(年/月/日)</div>
          <div>
          <!-- hidSELECTARRYMD / 日付データ -->
          <div id="calid"></div>
          <input type="text" name="cmbARRY" class="original_lodging_year" size="5" maxlength="4" onclick="cal.write();" onchange="cal.getFormValue(); cal.hide();">/
          <input type="text" name="cmbARRM" class="original_lodging_month" maxlength="2" onclick="cal.write();" onchange="cal.getFormValue(); cal.hide();">/
          <input type="text" name="cmbARRD" class="original_lodging_date" size="3" maxlength="2" onclick="cal.write();" onchange="cal.getFormValue(); cal.hide();">
          </div>
          <!-- hidSELECTHAKSU / 泊数データ -->
          <!-- Direct Inの設定に合わせて option を増減してください -->
          <select name="hidSELECTHAKSU">
            <option value="1" selected>1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option>
          </select>泊

          <!-- 予約室数、日程未定を非表示 -->
          <input type="hidden" name="hidROOM" value="0">
          <input type="checkbox" name="chkymd" id="chkymd" value="0" onclick="cngchkymd();" style="display:none;">
        </div>

        <div class="lodging_person"> 
          <div class="tit">ご宿泊人数(部屋/人)</div>

          <div>大人<select class="original_loading_adult" name="cmbADULT"><option value="1">1</option><option value="2" selected>2</option><option value="3">3</option><option value="4">4</option></select>名</div>
            
            <!-- 人数未定を非表示 -->
          <input type="checkbox" name="chkpsn" id="chkpsn" value="0" onclick="cngchkpsn();" style="display:none;">

          <div>子供<select class="original_loading_child" name="cmbCHILDa"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option></select>名</div>
          
          <!-- 子供B以降を非表示 -->
          <input type="hidden" name="cmbCHILDb" value="0"><input type="hidden" name="cmbCHILDc" value="0"><input type="hidden" name="cmbCHILDd" value="0">
                
          <!-- 料金検索 -->
          <input type="hidden" name="minPrice" value="0">
          <input type="hidden" name="maxPrice" value="9999999">
          <!-- Dispunit / 表示内容切替 -->
          <input type="hidden" name="hiddispunit" id="selectplan" value="" checked><label for="selectplan"></label>
        
        </div>

        <div class="lodging_submit"> 
          <!-- submit -->
          <input type="button" name="seek" class="original_btn_s" value="" onclick="btnSeekSubmit(); ga('send', 'event', 'click', 'searchbtn')">
          <input type="button" name="seek" class="original_btn_c" value="" onclick="btnSeekSubmitCancel()">

          </div>

          <input type="hidden" name="hidSELECTARRYMD" value="">
          <input type="hidden" name="hidchkymd" value="">
          <input type="hidden" name="hidchkpsn" value="">
          <input type="hidden" name="Dispunit" value="">
      </div><!-- END booking_inner //-->
      </form>

      <!-- Java Script プログラム --->
      <!-- 別ファイルとして呼び出す場合は、必ず form より下で呼び出すようにしてください --->
      <!-- すでにjqueryを利用されていても、jQueryの呼び出しは削除しないでください -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
      <script type="text/javascript" src="<?php info('home_url'); ?>js/jkl-calendar.js" charset="Shift_JIS"></script>
      <script type="text/javascript" src="<?php info('home_url'); ?>js/directin-search.js" charset="Shift_JIS"></script><script>
      <!--
        // 日付を自動設定
        // 初期値は、翌日宿泊日になっています
        // 当日の場合は、today.setDate(today.getDate() + 1)　を　today.setDate(today.getDate())　にしてください
          var objfrm =  document.frmplanseek

          today = new Date();
          today.setDate(today.getDate() + 1);

          objfrm.cmbARRY.value = today.getFullYear();
          objfrm.cmbARRM.value = today.getMonth() + 1;
          objfrm.cmbARRD.value = today.getDate();
      //-->
      </script>
      <!-- 宿泊検索システムここまで -->

      </div>
      <div class="lodging_present"><img src="<?php info('home_url'); ?>images/lodging_present.png"></div>
    </div>
    <!-- レストラン予約用の検索パネルができてから対応
    <div id="ryouri_panel"></div>
     -->

  </div>
</div>
<!-- headerここまで -->
