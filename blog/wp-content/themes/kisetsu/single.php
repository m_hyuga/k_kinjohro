<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();
?>
<div id="ContentsArea-index">
  <div id="ContentsArea">
 
<div id="MainEntry">

<h2 style="margin-bottom: 0"><img src="<?php info('home_url'); ?>images/news/common/img_01.png" width="199" height="445" alt="金城樓の季節のご案内" /></h2>
    </div>
<div id="RightEntry">



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<h3><?php $image_id = get_post_thumbnail_id();
					$image_url = wp_get_attachment_image_src($image_id, true);
					if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
					echo '<img src="'.$image_url[0].'" alt="'.get_the_title( ).'">';
					}
		?></h3>
<div class="MT10B30">
<?php the_content(); ?>
</div>
<?php if ( post_custom('day') or post_custom('kaihi') or post_custom('moshikomi')) : ?>
<div class="entry_detail">
<h4><img src="<?php info('home_url'); ?>/images/news/common/sukedule.png" width="651" height="41" alt="タイムスケジュール" /></h4>
<table>
	<?php 
	if ( post_custom('day') ) { // テキストの有無のチェック
		echo '<tr class="day"><th>日時</th><td>';
		echo nl2br(post_custom('day'));
		echo '</td></tr>';
	}
	if ( post_custom('kaihi') ) { // テキストの有無のチェック
		echo '<tr class="kaihi"><th>会費</th><td>';
		echo nl2br(post_custom('kaihi'));
		echo '</td></tr>';
	}
	if ( post_custom('moshikomi') ) { // テキストの有無のチェック
		echo '<tr class="moshikomi"><th>お申込方法</th><td>';
		echo nl2br(post_custom('moshikomi'));
		echo '</td></tr>';
		
	}
	
	echo '</table>';
	
	$imagefield = get_imagefield('filefield');
	if ( post_custom('filefield') ) {
		echo '<p class="btn_area"><a href="'.$imagefield['url'].'" target="_blank"><img src="';
		info('home_url');
		echo 'images/news/common/bt.png" width="197" height="53" alt="申込書ダウンロード"  class="MT5B30 Hover"/></a></p>';
	}
	?>
</div>
<?php endif; ?>

<div align="center">
</div>
<?php endwhile; endif; ?>



</div>
<div id="TopButton" class="clear"><img src="<?php info('home_url'); ?>images/office/01/img_16.jpg" width="106" height="29" alt="ページの先頭" onClick="backToTop(); return false;" onKeyPress="backToTop(); return false;" /></div>
  </div>
</div>
<?php get_footer();