<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<div id="footer_area">
  <div class="inner">
    <div id="footer_logo"><a href="<?php info('home_url'); ?>"><img src="<?php info('home_url'); ?>images/footer_logo.png" alt="金城楼"></a></div>
    <ul id="footernavi">
      <li><a class="ryouri" href="<?php info('home_url'); ?>office/ryouri.html">お食事</a></li>
      <li><a class="lodging" href="<?php info('home_url'); ?>office/lodging.html">ご宿泊</a></li>
      <li><a class="bridal" href="<?php info('home_url'); ?>wedding/index.html">ご婚礼</a></li>
      <li><a class="lunchbox" href="<?php info('home_url'); ?>group/syousai.html">お弁当</a></li>
      <li><a class="onlineshop" href="https://asp.hotel-story.ne.jp/ec/kinjohro/products/list.php" target="_blank">お取り寄せ</a></li>
    </ul>
    <ul id="footersubnavi">
      <li><a href="<?php info('home_url'); ?>form/index.html">ご予約・お問い合わせ</a></li>
      <li><a href="<?php info('home_url'); ?>others/index.html">会社概要</a></li>
      <li><a href="<?php info('home_url'); ?>others/recruit.html">採用情報</a></li>
      <li><a href="<?php info('home_url'); ?>others/access.html">交通アクセス</a></li>
      <li><a href="<?php info('home_url'); ?>others/link.html">リンク</a></li>
      <li><a href="<?php info('home_url'); ?>others/map.html">サイトマップ</a></li>
    </ul>
    <div id="footer_info">
      Copyright &copy; KINJOHRO CO., LTD. All rights reserved.
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7188449-3', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>
