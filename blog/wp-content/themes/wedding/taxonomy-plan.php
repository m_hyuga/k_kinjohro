<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<div id="MaineImg">

  <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_01.jpg" width="850" height="314" alt="" />
   <div id="SubMenu">
  <a href="http://www.kinjohro.co.jp/wedding/cocept.html"><img src="http://www.kinjohro.co.jp/images/wedding/02/img_06.png" width="141" height="46" class="iepngfix" alt="コンセプト" onmouseover="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_06_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_06.png',IEPNGFIX.fix(this)" /></a><!--
--><a href="<?php bloginfo('url'); ?>/plan/"><img src="http://www.kinjohro.co.jp/images/wedding/02/img_07.png" width="142" height="46" class="iepngfix" alt="ご婚礼プラン" onmouseover="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_07_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_07.png',IEPNGFIX.fix(this)" /></a><!--
--><a href="http://www.kinjohro.co.jp/wedding/dinner.html"><img src="http://www.kinjohro.co.jp/images/wedding/02/img_08.png" width="141" height="46" class="iepngfix" alt="お料理" onmouseover="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_08_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_08.png',IEPNGFIX.fix(this)" /></a><!--
--><a href="http://www.kinjohro.co.jp/wedding/hall.html"><img src="http://www.kinjohro.co.jp/images/wedding/02/img_09.png" width="143" height="46" class="iepngfix" alt="会場のご案内" onmouseover="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_09_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/wedding/02/img_09.png',IEPNGFIX.fix(this)" /></a>
</div>
</div>






<div id="ContentsArea-index">
  <div id="ContentsArea">
 
<div id="MainEntry">
       
       	  <h2><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_02.jpg" width="200" height="504" alt="ご婚礼プラン" /></h2>
          
            	
       
           
                    
        
      
    </div>
<div id="RightEntry">
        <div id="TxtEntry">
    <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_03.jpg" width="650" height="284" alt="真心こめてお手伝い。" class="MB30" />
    <br />
          

          <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_39.jpg" width="650" height="38" alt="金城樓オーダーメイドプラン" /></h3>
          
          <div id="centerBox">
<table border="0" cellpadding="0" cellspacing="0" class="Kplan">
 
 

<?php $posts=get_posts('numberposts=10 & post_type=plan2'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>


	 <tr>
    <th><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></th>
   <td><?php echo c2c_get_custom('アイコン', '<img class="iepngfix" alt="" src="http://www.kinjohro.co.jp/blog/wedding/files/', '" />'); ?></td>
  </tr>
<?php endforeach; endif; ?>   

</table>   


          <div class="clear"><br /></div>
          
          <br />
           </div>
           <br />
           <br />
          <br />
           <br />
          <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_38.jpg" width="650" height="38" alt="金城樓二次会プラン" /></h3>
          
          <div id="centerBox">
<table border="0" cellpadding="0" cellspacing="0" class="Kplan">
 
 

<?php $posts=get_posts('numberposts=10 & post_type=plan3'); ?>
<?php if ( $posts ) : foreach($posts as $post) : setup_postdata($post); ?>


	 <tr>
    <th><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></th>
   <td><?php echo c2c_get_custom('アイコン', '<img class="iepngfix" alt="" src="http://www.kinjohro.co.jp/blog/wedding/files/', '" />'); ?></td>
  </tr>
<?php endforeach; endif; ?>   

</table>  
          <div class="clear"><br /></div>
          
          
          <br />
           </div>
        </div>
        
        <br />
        
        
        
        <!-- <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_32.jpg" width="651" height="37" alt="5月〜8月限定プラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_33.jpg" width="293" height="184" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_37.jpg" width="305" height="33" alt="縁" /><br />
   <p class="Li24"><br />
     古都金沢の老舗料亭だからこその心くばりとおもてなしをテーマにした、５～８月までの４ヶ月間の特別プラン。<br />
     一会場一組限定なので、お好きな時間に始めることができ<br />
     ゆとりあるスケジュールで優雅な雰囲気を楽しめます。<br />
     さらに、嬉しい選べる特典でお二人らしい結婚式を演出してみませんか。
<br />
<br />
【上記料金には、それぞれ下記の料金が含まれております】
<br />
お料理・お飲物・席札・献立表・お席料・親族控室・着付け室・介添え・メイン装花・卓上花・奉仕料・税金</p>
<br />
<div class="clear"><br /></div>
 <br />
      <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_34.jpg" width="292" height="18" alt="特典" class="MB20" />
            <br />
	  <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_35.jpg" width="345" height="99" class="LImg00" alt="A" />
      
       <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_36.jpg" width="277" height="163" class="RImg00" alt="B" /> 


   <br />
<div class="clear"><br /></div>
<br />
  
        <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_10.jpg" width="650" height="38" alt="ご婚礼プラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_12.jpg" width="291" height="160" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_11.jpg" width="315" height="72" alt="契" /><br />
   <p class="Li24"><br />
花色に染められて、今、わたしはしあわせ色。<br />
専門のフラワーコーディネーターがお二人の希望を叶えます。</p>
<br />
<div class="clear"><br /></div>
 <br />
        
        
	  <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_13.jpg" width="302" height="183" class="LImg" alt="" />
      <div class="RBoxP">
       <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_14.jpg" width="311" height="70" alt="絆" /> 
   <p><br />
玄関では｢お水合せ｣の儀式から。<br />
花嫁のれんをくぐって披露宴会場へ。<br />
母から教わったことをちゃんと伝えていきたい。<br />
金沢ならではの金城樓がいつまでも伝えていきたいご結婚式です。</p>
</div>
<br />
<div class="clear"><br /></div>
<br />
  
        <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_15.jpg" width="650" height="37" alt="特別少人数プラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_17.jpg" width="291" height="146" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_16.jpg" width="309" height="71" alt="結" /><br />
   <p class="Li24"><br />
     お庭を眺めながら・・・<br />
     ゆっくりとした時間の中でゆかしき「末広の間」での結婚式。</p>
<br />
<div class="clear"><br /></div>
 <br />
        
        
	  <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_19.jpg" width="295" height="167" class="LImg" alt="" />
      <div class="RBoxP">
       <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_18.jpg" width="311" height="66" alt="夢" /> 
   <p><br />
     ロマンティックでドラマティックなきらめき。<br />
     静かにまたたくキャンドルが幸せを照らす光の演出・・・<br />
     夕方から宵にかけての新｢WA｣感覚のオリジナリティ<br />
     溢れる祝宴です。</p>
</div>
<br />
<div class="clear"><br /></div>
<br />
     <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_20.jpg" width="650" height="37" alt="前撮りプラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_22.jpg" width="173" height="162" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_21.jpg" width="410" height="18" alt="一生に一度の花嫁姿を最高のロケーションで" /><br />
   <p class="Li24"><br />
     金城楼ならではの本物の和にこだわったご披露宴のご提案。<br />
     芸妓さんの手配や、和の雰囲気を大切にした演出もご安心してお任せください。<br />
     ■含まれるもの<br />
     料理、飲物、控室、介添料、音響、司会者、招待状、席次表、席札、お品書き、<br />
     芳名帳、鏡開き、芸妓、会場装花、写真、席料、サービス料、税金<br />
</p>
<div class="clear"><br />

</div>
<br />
  <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_23.jpg" width="650" height="38" alt="前撮りプラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_24.jpg" width="285" height="117" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_25.jpg" width="330" height="17" alt="一生に一度の花嫁姿を最高のロケーションで" /><br />
   <p class="Li24"><br />
     由緒あるお部屋、風情漂う日本庭園<br />
和装の映えるロケーションで最高の花嫁姿を…<br />
■含まれるもの<br />
お着付室（トイレ・バス付）、写真場、ランチ2名様</p>
<br />

<div class="clear"><br /></div>
<br />
    <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_26.jpg" width="650" height="38" alt="人前式プラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_28.jpg" width="209" height="148" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_27.jpg" width="364" height="42" alt="人前挙式、ウェルカムパーティーで たくさんの人に祝福されるあたたかウェディングを" /><br />
<p class="Li24"><br />
    お2人だけのこだわりの人前式をしませんか？<br />
雰囲気、演出ご希望に合わせてご提案させていただきます。<br />
■含まれるもの<br />
人前挙式、ウェルカムパーティ、折鶴シャワー、番傘レンタル、<br />
料理、飲物、控室、介添料、音響、司会者、招待状、席次表、<br />
席札、お品書き、芳名帳、鏡開きor生ケーキ、会場装花、写真、<br />
席料、サービス料、税金</p>
<br />

<div class="clear"><br /></div>
<br />
  <h3><img src="http://www.kinjohro.co.jp/images/wedding/plan/img_29.jpg" width="650" height="37" alt="２次会プラン" class="MB30" /></h3>
        
         <img src="http://www.kinjohro.co.jp/images/wedding/plan/img_31.jpg" width="209" height="148" class="RImg" alt="" /> 
   		<img src="http://www.kinjohro.co.jp/images/wedding/plan/img_30.jpg" width="390" height="18" alt="ご宿泊プレゼントつきでご披露宴後もゆっくり楽しむ" /><br />
<p class="Li24"><br />
    お人数、ご予算に合わせてお料理、お飲物ご用意いたします。<br />
料理：オードブルorビュッフェスタイル<br />
飲み物；フリードリンク（日本酒・ビール・ソフトドリンク）<br />
■含まれるもの<br />
料理、飲み物、席料、サービス料、控え１室</p>
<br />

<div class="clear"><br /></div>-->
<br />
         <div align="center"><a href="http://www.kinjohro.co.jp/form/form2.html"><img src="http://www.kinjohro.co.jp/images/form/img_44.jpg" width="197" height="48" alt="お問い合わせ・資料請求" class="Hover MB20" /></a><br />
              </div>
<br />
   </div>
<div id="TopButton" class="clear"><img src="http://www.kinjohro.co.jp/images/office/01/img_16.jpg" width="106" height="29" alt="ページの先頭" onclick="backToTop(); return false;" onkeypress="backToTop(); return false;" /></div>
     
     
     
     
  </div>
</div>








<?php get_footer(); ?>


