<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php  if(is_home()) : // ホームなら  ?>

<div id="MaineImg">

  <img src="http://www.kinjohro.co.jp/images/wedding/01/img_18.jpg" width="850" height="316" alt="" /></div>
<div id="ContentsArea-index">
  <div id="ContentsArea">
 
<?php get_sidebar(); ?>
<div id="RightEntry">
        <div id="TxtEntry">
       	  <h3><img src="http://www.kinjohro.co.jp/images/wedding/01/img_03.jpg" width="400" height="64" alt="気品たかく、香りたかく、自分たちらしく。金城樓のご婚礼。" /></h3>
          <div id="ImgTOP"><img src="http://www.kinjohro.co.jp/images/wedding/01/img_04.png" width="169" height="222" alt="" class="iepngfix" /></div>
          
   	  <p>数えきれないほどの祝福の言葉とあたたかな拍手につつまれて、永遠の愛を誓いあう日。<br />
   	    その喜びをいつまでも色あせない感動とするために、<br />
   	    私どもはお二人の新しい門出を雅びやかに、華やかに祝します。<br />
   	    日本のベーシックな様式を大切にしながら、お二人の思いを細やかに叶える金城樓のご婚礼。<br />
   	    気品たかく、香りたかく、自分たちらしく。<br />
   	    そんな慶びのかたちづくりを私どもは真心こめてお手伝いしてまいります。</p>
      </div>

      <table border="0" cellpadding="0" cellspacing="0" class="Wnews">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	 <tr>
    <td class="td01"><a href="http://zexy.net/zhpd/wedding/fair/date/c_7770023238"><img src="<?php bloginfo('template_url'); ?>/images/img/img_03.jpg" width="362" height="52" alt="ブライダルフェア" /></a></td>
    <td></td>
  </tr>
     
<?php endwhile; // end of the loop. ?>

</table>
<br />

      
      
      <table class="TablePDFW" border="0">
  <tr>

    <td class="PV"><a href="javascript:;" onclick="window.open('http://www.kinjohro.co.jp/pv/page02.html','win1','width=756,height=690');">ブライダルのご紹介（北陸朝日放送より）</a></td>
    
    <td align="right"></td>
    
    </tr>
</table>
      <div>
      <p></p>
      </div>
      
      
      <div class="ImgEntry">
      
      <div class="ImgBOX">
      <h4><img src="http://www.kinjohro.co.jp/images/wedding/01/img_04.jpg" width="153" height="31" alt="コンセプト" /></h4>
      
      <a href="http://www.kinjohro.co.jp/wedding/cocept.html"><img src="http://www.kinjohro.co.jp/images/wedding/01/img_05.jpg" width="153" height="117" class="Hover" alt="" /></a></div>
      
      <div class="ImgBOX M15P">
      <h4><img src="http://www.kinjohro.co.jp/images/wedding/01/img_06.jpg" width="153" height="31" alt="ご婚礼プラン" /></h4>
      
      <a href="<?php bloginfo('url'); ?>/plan/"><img src="http://www.kinjohro.co.jp/images/wedding/01/img_07.jpg" width="153" height="117" class="Hover" alt="" /></a></div>
      <div class="ImgBOX M15P">
      <h4><img src="http://www.kinjohro.co.jp/images/wedding/01/img_08.jpg" width="153" height="31" alt="お料理" /></h4>
      
      <a href="http://www.kinjohro.co.jp/wedding/dinner.html"><img src="http://www.kinjohro.co.jp/images/wedding/01/img_09.jpg" width="153" height="117" class="Hover" alt="" /></a></div>
      <div class="ImgBOX2">
      <h4><img src="http://www.kinjohro.co.jp/images/wedding/01/img_10.jpg" width="153" height="31" alt="会場のご案内" /></h4>
      
      <a href="http://www.kinjohro.co.jp/wedding/hall.html"><img src="http://www.kinjohro.co.jp/images/wedding/01/img_11.jpg" width="153" height="117" class="Hover" alt="" /></a></div>
      <div class="clear"></div>
      </div>
     
    </div>
    <div id="TopButton" class="clear"><img src="http://www.kinjohro.co.jp/images/office/01/img_16.jpg" width="106" height="29" alt="ページの先頭" onclick="backToTop(); return false;" onkeypress="backToTop(); return false;" /></div>
     
     
     
     
  </div>
</div>








<?php get_footer(); ?>
  <?php elseif(get_post_type() === 'plan'): ?>
<?php include (TEMPLATEPATH . '/taxonomy-plan.php'); ?>

  <?php elseif(get_post_type() === 'plan2'): ?>
  
  <?php include (TEMPLATEPATH . '/404.php'); ?>
<?php else : ?>
<?php endif;	// is_home() ?>