<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

 <div id="footer">
  <div id="footerContents">
  <div class="LogoArea">
  <img src="/images/index/img_logo2.png" width="93" height="36" alt="金城樓" class="iepngfix" /></div>
  <div class="LeftArea">
  <img src="/images/index/img_07.png" width="344" height="26" alt="金城樓本店" class="iepngfix" />
  <img src="/images/index/img_10.png" width="360" height="16" alt="天麩羅専門店 天金" class="iepngfix" />
  </div>
 
<div class="RightArea">
   <img src="/images/index/img_33.png" width="330" height="27" alt="ブライダル部門" class="iepngfix" />
  <img src="/images/index/img_30.png" width="330" height="26" alt="オンラインショップ" class="iepngfix" />
  
  </div>
  
  <div id="footerClear"><img src="/images/index/img_08.png" width="270" height="11" alt="Copyright © 2009 KINJOHRO CO., LTD. All rights reserved " class="iepngfix" /></div>
  </div>
  

  
  
  </div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>

</body>


</html>





