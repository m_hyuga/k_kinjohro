<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Links
*/
?>
<?php switch_to_blog(11); ?>
<p><a href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?> <?php the_title(); ?></a></p>         
