<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<link rel="shortcut icon" href="http://www.kinjohro.co.jp/images/index/kinjohro.ico" />
<link href="http://www.kinjohro.co.jp/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="http://www.kinjohro.co.jp/css/common.css" rel="stylesheet" type="text/css" media="all" />

<link href="http://www.kinjohro.co.jp/css/office.css" rel="stylesheet" type="text/css" media="all" />
<script language='JavaScript' src='http://www.kinjohro.co.jp/js/pagetop.js' type="text/javascript"></script>
<script language='JavaScript' src='http://www.kinjohro.co.jp/js/iepngfix.js' type="text/javascript"></script>
<!-- プルダウン -->
<script type='text/javascript' src='http://www.kinjohro.co.jp/js/jquery.js'></script>    
<script type='text/javascript' src='http://www.kinjohro.co.jp/js/jquery.droppy.js'></script>
<link rel="stylesheet" href="http://www.kinjohro.co.jp/css/pull/droppy.css" type="text/css" />
<script language='JavaScript' src='http://www.kinjohro.co.jp/js/jquery_auto.js' type="text/javascript"></script>


</head>
<body>
<div id="wrapperPage">
  <div id="HeadArea">
	  <div id="HeadL">
      	<h1 id="toplist">
       	  <a href="http://www.kinjohro.co.jp"><img src="http://www.kinjohro.co.jp/images/index/img_logo.png" width="118" height="149" alt="金城楼" class="iepngfix" /></a> </h1>
    </div>
  <div id="HeadR2">
     <ul>
     <li><a href="http://www.kinjohro.co.jp/english/index.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_03_2.png" width="43" height="15" alt="English" class="iepngfix" /></a></li>
     <li class="Ma20"><a href="http://www.kinjohro.co.jp/form/index.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_17.png" width="110" height="15" alt="ご予約・お問い合わせ" class="iepngfix" /></a></li>
  <li class="Ma20"><a href="http://www.kinjohro.co.jp/others/index.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_03_1.png" width="48" height="15" alt="会社概要" class="iepngfix" /></a></li>
       <li class="Ma20"><a href="http://www.kinjohro.co.jp/others/recruit.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_02.png" width="47" height="14" alt="採用情報" class="iepngfix" /></a></li>
       <li class="Ma20"><a href="http://www.kinjohro.co.jp/others/access.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_01.png" width="67" height="14" alt="交通アクセス" class="iepngfix" /></a></li>
       <li class="Ma20"><a href="http://www.kinjohro.co.jp/others/link.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_03.png" width="30" height="14" alt="リンク" class="iepngfix" /></a></li>

       <li class="Ma20"><a href="http://www.kinjohro.co.jp/others/map.html"><img src="http://www.kinjohro.co.jp/images/office/01/img_04.png" width="61" height="14" alt="サイトマップ" class="iepngfix" /></a></li>

      </ul>
    </div>
       
</div>


<div id="PanKuzu">
  <div id="Pan">
  <span class="Pankuzu"><a href="http://www.kinjohro.co.jp/">ホーム</a> > <a href="http://www.kinjohro.co.jp/office/index.html">金城樓本店のご案内</a> > <a href="<?php bloginfo('url'); ?>">浅の川亭</a></span>  
  </div>



</div>



<div id="wrapperMenu">


<div class="MenuA">
<ul id='nav'>
  
  <li class="ButtonMT"><a href="http://www.kinjohro.co.jp/kodawari/index.html"><img class="iepngfix" src="http://www.kinjohro.co.jp/images/index/01/img_01_off.png" onmouseover="this.src='http://www.kinjohro.co.jp/images/index/01/img_01_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/index/01/img_01_off.png',IEPNGFIX.fix(this)" width="120" height="32" alt="金城樓のこだわり" /></a>
    <ul>
      <li><a href="http://www.kinjohro.co.jp/kodawari/history.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull01off_01.png" width="123" height="32" alt="金城樓の歴史" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01on_01.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01off_01.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/kodawari/millennium.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull01off_02.png" width="123" height="30" alt="浅野川年代記" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01on_02.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01off_02.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/kodawari/osechi.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull01off_03.png" width="123" height="38" alt="金城樓のおせち" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01on_03.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull01off_03.png',IEPNGFIX.fix(this)"  /></a></li>

  
    </ul>
  </li>
  
  <li class="ButtonM"><a href="http://www.kinjohro.co.jp/office/index.html"><img class="iepngfix" src="http://www.kinjohro.co.jp/images/index/menu/img_02_off.png" onmouseover="this.src='http://www.kinjohro.co.jp/images/index/menu/img_02_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/index/menu/img_02_off.png',IEPNGFIX.fix(this)" width="125" height="32" alt="金城樓本店のご案内" /></a>
    <ul>
      <li><a href="http://www.kinjohro.co.jp/office/ryouri.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_01.png" width="123" height="32" alt="お料理" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_01.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_01.png',IEPNGFIX.fix(this)"  /></a></li>
          <li><a href="http://www.kinjohro.co.jp/kodawari/gift.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_07.png" width="123" height="31" alt="ショッピング" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_07.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_07.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/office/tea.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_02.png" width="123" height="30" alt="料亭と茶屋" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_02.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_02.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/blog/beerhall/"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_03.png" width="123" height="31" alt="浅の川ビアホール" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_03.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_03.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="<?php bloginfo('url'); ?>"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_04.png" width="123" height="31" alt="浅の川亭" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_04.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_04.png',IEPNGFIX.fix(this)"  /></a></li>

      <li><a href="http://www.kinjohro.co.jp/office/lodging.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_05.png" width="123" height="30" alt="ご宿泊" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_05.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_05.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/office/building.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull02off_06.png" width="123" height="38" alt="館内のご案内" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02on_06.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull02off_06.png',IEPNGFIX.fix(this)"  /></a></li>
    </ul>
  </li>
  
  <li class="ButtonM"><a href="http://www.kinjohro.co.jp/blog/wedding/"><img class="iepngfix" src="http://www.kinjohro.co.jp/images/index/menu/img_03_off.png" onmouseover="this.src='http://www.kinjohro.co.jp/images/index/menu/img_03_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/index/menu/img_03_off.png',IEPNGFIX.fix(this)" width="115" height="32" alt="金城樓のブライダル" /></a>
    <ul>
      <li><a href="http://www.kinjohro.co.jp/wedding/cocept.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull03off_01.png" width="123" height="32" alt="コンセプト" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03on_01.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03off_01.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/blog/wedding/plan/"><img src="http://www.kinjohro.co.jp/images/office/pull/pull03off_02.png" width="123" height="30" alt="ご婚礼プラン" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03on_02.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03off_02.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/wedding/dinner.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull03off_03.png" width="123" height="31" alt="お料理" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03on_03.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03off_03.png',IEPNGFIX.fix(this)"  /></a></li>

      <li><a href="http://www.kinjohro.co.jp/wedding/hall.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull03off_04.png" width="123" height="31" alt="会場のご案内" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03on_04.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03off_04.png',IEPNGFIX.fix(this)"  /></a></li>
            <li><a href="http://www.kinjohro.co.jp/wedding/simulation.php"><img src="http://www.kinjohro.co.jp/images/office/pull/pull03off_05.png" width="123" height="38" alt="見積シミュレーション" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03on_05.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull03off_05.png',IEPNGFIX.fix(this)"  /></a></li>
    </ul>
  </li>
  
  <li class="ButtonM"><a href="http://www.kinjohro.co.jp/kodawari/gift.html"><img class="iepngfix" src="http://www.kinjohro.co.jp/images/index/menu/img_05_off.png" onmouseover="this.src='http://www.kinjohro.co.jp/images/index/menu/img_05_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/index/menu/img_05_off.png',IEPNGFIX.fix(this)" width="87" height="32" alt="ショッピング" /></a>  </li>
  
  <li class="ButtonM2"><a href="http://www.kinjohro.co.jp/group/index.html"><img class="iepngfix" src="http://www.kinjohro.co.jp/images/index/menu/img_04_off.png" onmouseover="this.src='http://www.kinjohro.co.jp/images/index/menu/img_04_on.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/index/menu/img_04_off.png',IEPNGFIX.fix(this)" width="117" height="32" alt="ブループ店のご案内" /></a>
   <ul>
      <li><a href="http://www.kinjohro.co.jp/blog/group/"><img src="http://www.kinjohro.co.jp/images/office/pull/pull04off_01.png" width="123" height="32" alt="かりん庵" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04on_01.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04off_01.png',IEPNGFIX.fix(this)"  /></a></li>
      
      <li><a href="http://www.kinjohro.co.jp/group/syousai.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull04off_03.png" width="123" height="31" alt="旬菜工房" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04on_03.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04off_03.png',IEPNGFIX.fix(this)"  /></a></li>
      <li><a href="http://www.kinjohro.co.jp/office/index.html"><img src="http://www.kinjohro.co.jp/images/office/pull/pull04off_04.png" width="123" height="38" alt="金城樓本店" class="iepngfix" onmouseover="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04on_04.png',IEPNGFIX.fix(this)" onmouseout="this.src='http://www.kinjohro.co.jp/images/office/pull/pull04off_04.png',IEPNGFIX.fix(this)"  /></a></li>

    </ul> 
  </li>

  
    
</ul>

<script type='text/javascript'>
  $(function() {
    $('#nav').droppy();
  });
</script>

  <div class="clear"></div>
</div>
  <div class="clear"></div>
</div>
</div>







