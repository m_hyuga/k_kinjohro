<?php
/*
Plugin Name: TaM Google Analytics
Plugin URI: http://ideasilo.wordpress.com/2006/10/25/tam-google-analytics/
Description: The simplest Google Analytics tracking code installer.
Version: 1.0
Author: Takayuki Miyoshi
Author URI: http://ideasilo.wordpress.com
*/

class tam_google_analytics {

  function tam_google_analytics() {
    add_action('init', array(&$this, 'discharge_cargos'));
    add_action('wp_footer', array(&$this, 'insert_tracking_code'), 100);
    add_action('admin_menu', array(&$this, 'add_pages'));
  }
  
  function insert_tracking_code() {
    global $current_user;
    if ($current_user->ID) {
      $role = array_shift($current_user->roles);
      echo "<!-- No Google Analytics tracking code because you are login now as $role of this blog. -->\n";
    } else {
      echo "<!-- Google Analytics tracking code -->\n";
      echo stripslashes(get_option('tamga_tracking_code')) . "\n\n";
    }
  }
  
  function add_pages() {
		add_options_page('Google Analytics', 'Google Analytics', 'manage_options', __FILE__, array(&$this, 'option_page'));
	}
  
  function option_page() {
?>
<div class="wrap">
  <h2>Copy Your Google Analytics Tracking Code And Paste Here</h2>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF'] . '?page=' . basename(__FILE__); ?>">
    <textarea name="tamgatrackingcode" rows="20" cols="50" style="width: 98%;"><?php echo stripslashes(get_option('tamga_tracking_code')); ?></textarea>
    <p class="submit"><input type="submit" value="Save &raquo;" /></p>
  </form>
</div>
<?php
  }
  
  function discharge_cargos() {
    if (isset($_POST['tamgatrackingcode'])) {
      update_option('tamga_tracking_code', $_POST['tamgatrackingcode']);
    }
  }
  
}

new tam_google_analytics();
  
?>